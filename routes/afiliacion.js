var express = require('express');
var app = express();

var Afiliacion = require('../models/afiliacion');
var Proveedor = require('../models/proveedor');
var Servicio = require('../models/servicio');
var mdAutentication = require('../middlewares/autenticacion');

// Rutas 
/* Obtener Todas las afiliaciones */
app.get('/', (req, res, next) => {
   Afiliacion.find({})
   .populate('servicio', 'nombre img estatus')
   .populate('proveedor', 'nombre imagen')
   .populate('producto', 'nombre imagen precio categoria estatus ingrediente')
        .exec(
            (err, afiliacion) => {
                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando afiliación.',
                        errors: err
                    })
                } res.status(200).json({
                    ok: true,
                    Afiliacion: afiliacion
                });
            })
});


/* Obtener afiliacion por parametro */
app.get('/:busqueda',mdAutentication.verificaToken, (req, res, next) => {

    var busqueda = req.params.busqueda;   
 
    Afiliacion.find({servicio:{$in:[busqueda]}})
    .populate('servicio', 'nombre imagen')
    .populate('proveedor', 'nombre imagen')
    .populate('producto', 'nombre imagen precio categoria estatus ingrediente')
    .exec(      
        (err,afiliacion) => {
        if (err){ 
            return res.status(500).json({
                ok: false,
                mensaje: 'Error cargando afiliación',
                errors: err
            })
            }else {
                res.status(200).json({
                    ok: true,
                    afiliacion: afiliacion
                });
            }
                });
});

/* Crear nueva afiliacion */
app.post('/', mdAutentication.verificaToken, (req, res) => {
    var body = req.body;
    var date = new Date()
    var month= date.getMonth()+1;
    var day = date.getDate();  
    var hour = date.getHours();
    var minut= date.getMinutes();
    var secon = date.getSeconds();

    var codAfiliacion = hour.toString()+ minut.toString()+ secon.toString()+month.toString()+day.toString();


        var afiliacion = new Afiliacion({
            codAfiliacion:codAfiliacion,
            servicio: body.servicio,
            proveedor: body.proveedor,
            producto: body.productos,
            estatus: body.estatus       
        })

        afiliacion.save((err, afiliacionGuardada) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al crear afiliación',
                    errors: err
                })
            }
            res.status(201).json({
                ok: true,
                afiliacion:afiliacionGuardada,
                mensaje:"Afiliacion Registrada"
            });
    
        });
 

});


/* Borrar direccion por ID */
app.delete('/:id', mdAutentication.verificaToken, (req, res) => {
    var id = req.params.id

    Afiliacion.findByIdAndRemove(id, (err, afiliacionBorrada) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al borrar afiliacion',
                errors: err
            })
        }

        if (!afiliacionBorrada) {
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe afiliacion',
                errors: { message: 'No existe afiliacion' }
            })
        }

        res.status(200).json({
            ok: true,
            mensaje: 'Afiliacion eliminada',
            servicio: afiliacionBorrada,
          //  usuariotoken: req.usuario
        });
    })
});
module.exports = app;