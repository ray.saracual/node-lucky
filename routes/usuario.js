var express = require('express');
var app = express();
var Usuario = require('../models/usuario');
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken')
var mdAutentication = require('../middlewares/autenticacion');
var EmailCtrl = require('./emailRegistro')
var nodemailer = require("nodemailer");
var hbs = require("nodemailer-express-handlebars");


/* Obtener Todos los Usuarios */
app.get('/',(req, res, next) => {

    Usuario.find({}, ' nombre telefono img email role valido')
        .exec(
            (err, usuarios) => {

                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando usuario',
                        errors: err
                    })

                } res.status(200).json({
                    ok: true,
                    usuarios: usuarios
                });

            })

});

/* Crear nuevos Usuarios */
app.post('/', (req, res) => {
    var body = req.body;

    var usuario = new Usuario({
        nombre: body.nombre,
        telefono: body.telefono,
        img: body.img,
        email: body.email,
        password: bcrypt.hashSync(body.password, 10),
        rol: body.rol
    })

    usuario.save((err, usuarioGuardado) => {
        if (err) {
            return res.status(400).json({
                mensaje: 'Error al crear usuario',
                errors: err
            })
        }
        var smtpConfig = {
            host: "smtp.gmail.com",
            port: 465,
            secure: true, // use SSL
            auth: {
              user: "gestion.saracual@gmail.com",
              pass: "alananer10"
            }
          };
        


  // Creo transporter
    let transporter = nodemailer.createTransport(smtpConfig);
    const handlebarOptions = {
      viewEngine: {
        extName: ".handlebars",
        partialsDir: "./templates/",
        layoutsDir: "./templates/",
        defaultLayout: "registroUsuario.handlebars"
      },
      viewPath: "./templates/",
      extName: ".handlebars"
    };
 

    transporter.use("compile", hbs(handlebarOptions));

    // verify connection configuration
    transporter.verify(function(error) {
      if (error) {
        console.log(error);
        return error;
      } else {
        console.log("Server is ready to take our messages");
      }
    });


   // Definimos el email
  var mailOptions = {
    from: '"Fastzoom"<gestiones@fastzoom.cl>',
    to: usuarioGuardado.email,
    subject: "Registro Fastzoom",
    attachments: [
      {
        filename: "fast-zoom-logosinfondo.png",
        path: "./assets/fast-zoom-logosinfondo.png",
        cid: "gestiones@fastzoom.cl"
      }
    ],
    template: "registroUsuario",
    context: {
      data: usuarioGuardado,
      ruta: "https://intense-dusk-29965.herokuapp.com/#/activar/" + usuarioGuardado._id
    } // send extra values to

  };



    // Enviamos el email
    transporter.sendMail(mailOptions, function(error, info) {
      if (error) {
        res.status(401).json({
            ok: false,
            mensaje: error,
                });
        console.log(error);
        // res.json(error);
      } else {
        console.log("Email sent");
        res.status(201).json({
            ok: true,
            mensaje: ' Usuario ' + usuarioGuardado.email + ' registrado.',
            usuariotoken: req.usuario
    });
      }
  
       // EmailCtrl.sendEmail(usuarioGuardado,'registroUsuario');

        // res.status(201).json({
        //     ok: true,
        //     mensaje: ' Usuario ' + usuarioGuardado.email + ' registrado.',
        //    // usuario: usuarioGuardado,
        //     usuariotoken: req.usuario
         });


    });

});


/* Actualizar Usuario */
app.put('/:id', mdAutentication.verificaToken, (req, res) => {
    var id = req.params.id
    var body = req.body;

    Usuario.findById(id, (err, usuario) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar usuario',
                erros: err
            });
        }

        if (!usuario) {
            return res.status(400).json({
                ok: false,
                mensaje: 'El usuario con el id: ' + id + 'no existe',
                erros: { message: 'No existe un usuario con es ID' }
            });
        }

            usuario.nombre = body.nombre,
            usuario.telefono = body.telefono,
            usuario.email = body.email,
            usuario.role = body.role

        usuario.save((err, usuarioGuardado) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al actualizar usuario',
                    erros: err
                });
            }

            usuarioGuardado.password = ':)',
                res.status(200).json({
                    ok: true,
                    usuario: usuarioGuardado,
                    usuariotoken: req.usuario
                });
        });
    });
});


/* Restablecer clave */
app.put('/reset-clave/:id', (req, res) => {
    var id = req.params.id
    var body = req.body;

    console.log(body)


    Usuario.findById(id, (err, usuario) => {
        if (err) {
            return res.status(404).json({
                ok: false,
                mensaje: 'El usuario con el id: ' + id + 'no existe',
                erros: err
            });
        }

        if (!usuario) {
            return res.status(400).json({
                ok: false,
                mensaje: 'El usuario con el id: ' + id + 'no existe',
                erros: { message: 'No existe un usuario con es ID' }
            });
        }

            usuario.password = bcrypt.hashSync(body.password, 10),
            // usuario.telefono = body.telefono,
            // usuario.email = body.email,
            // usuario.role = body.role

        usuario.save((err, usuarioGuardado) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al actualizar usuario',
                    erros: err
                });
            }

            usuarioGuardado.password = ':)',
                res.status(200).json({
                    ok: true,
                    mensaje: 'Su contraseña ha sido cambiada',
                });
        });
    });
});



/* validar Usuario */
app.put('/validar/:id', (req, res) => {
    var id = req.params.id
    var body = req.body;

    Usuario.findById(id, (err, usuario) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al validar usuario.',
                erros: err
            });
        }

        if (!usuario) {
            return res.status(404).json({
                ok: false,
                mensaje: 'Error al validar usuario.',
                erros: { message: 'No existe un usuario con es ID' }
            });
        }
           
            usuario.valido = true;

        usuario.save((err, usuarioGuardado) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al activar usuario. Intente de nuevo.',
                    erros: err
                });
            }

                res.status(200).json({
                    ok: true,
                    mensaje: 'Su usuario ha sido activado.',
                });
        });
    });
});


/* Borrar usuario por ID */
app.delete('/:id', mdAutentication.verificaToken, (req, res) => {
    var id = req.params.id

    Usuario.findByIdAndRemove(id, (err, usuarioBorrado) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al borrar usuario',
                errors: err
            })
        }

        if (!usuarioBorrado) {
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe un usuario con ese ID',
                errors: { message: 'No existe un usuario con ese ID' }
            })
        }

        res.status(200).json({
            ok: true,
            usuario: usuarioBorrado,
            usuariotoken: req.usuario
        });
    })
});
module.exports = app;