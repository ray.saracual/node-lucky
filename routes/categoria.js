var express = require('express');
var app = express();

var Categoria = require('../models/categoria');

var mdAutentication = require('../middlewares/autenticacion');

// Rutas 
/* Obtener Todos los categorias */
app.get('/', (req, res, next) => {
    Categoria.find({},)
        .exec(
            (err, categorias) => {
                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando categorias',
                        errors: err
                    })
                } res.status(200).json({
                    ok: true,
                    categorias: categorias
                });
            })
});



/* Crear nuevo categorias */
app.post('/',mdAutentication.verificaToken, (req, res) => {
    var body = req.body;
    var categoria = new Categoria({
        nombre: body.nombre,
        img: body.img,
    })



    categoria.save((err, categoriaGuardado) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear categoria',
                errors: err
            })
        }
        res.status(201).json({
            ok: true,
            categoria: categoriaGuardado
        });

    });

});



/* Actualizar categoria */
app.put('/:id',mdAutentication.verificaToken, (req, res) => {
    var id = req.params.id
    var body = req.body;

    Categoria.findById(id, (err, categoria) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar categoria',
                erros: err
            });
        }

        if (!categoria) {
            return res.status(400).json({
                ok: false,
                mensaje: 'El categoria con el id: ' + id + 'no existe',
                erros: { message: 'No existe un categoria con este ID' }
            });
        }

        categoria.nombre= body.nombre,
        categoria.img = body.img,

        categoria.save((err, categoriaGuardado) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al actualizar categoria',
                    erros: err
                });
            }

                res.status(200).json({
                    ok: true,
                    categoria: categoriaGuardado,
                    usuariotoken: req.usuario
                });
        });
    });
});


/* Borrar categoria por ID */
app.delete('/:id',mdAutentication.verificaToken, (req, res) => {
    var id = req.params.id

    categoria.findByIdAndRemove(id, (err, categoriaBorrado) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al borrar categoria',
                errors: err
            })
        }

        if (!categoriaBorrado) {
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe un categoria con ese ID',
                errors: { message: 'No existe un categoria con ese ID' }
            })
        }

        res.status(200).json({
            ok: true,
            mensaje: 'categoria borrado',
            categoria: categoriaBorrado,
            usuariotoken: req.usuario
        });
    })
});
module.exports = app;