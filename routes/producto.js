var express = require('express');
var app = express();
const fs = require('fs');
const path = require('path');


var Producto = require('../models/producto');

var mdAutentication = require('../middlewares/autenticacion');


// Rutas 

/* Obtener Todos los Productos*/
app.get('/', (req, res, next) => {

    Producto.find({})
    .populate('proveedor', 'nombre')
    .populate('categoria', 'nombre img')
        .exec(
            (err, productos) => {


                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando producto',
                        errors: err
                    })

                } 

                if (productos.length===0) {
                    return res.status(404).json({
                        ok: false,
                        mensaje: 'No se han registrado productos. Favor registre',
                        errors: err
                    })
                } 
                res.status(200).json({
                    ok: true,
                    productos: productos
                });
            })
});
/* Obtener Producto segun Proveedor */
app.get('/:proveedor', (req, res, next) => {
    Producto.find({proveedor})
        .populate('proveedor', 'nombre')
        .populate('categoria', 'nombre')
        .exec(
            (err, productos) => {

                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando producto',
                        errors: err
                    })

                } res.status(200).json({
                    ok: true,
                    productos: productos
                });

            })
});


/* Crear nuevos Productos */
app.post('/', mdAutentication.verificaToken, (req, res) => {

    var date = new Date();
    var miliSecond  = date.getMilliseconds();
    var body = req.body;
    var producto = new Producto({
        nombre: body.nombre,
        imagen: body.imagen,      
        categoria: body.categoria,
        ingrediente :body.ingrediente,
        precio: body.precio,
        estatus: body.estatus,
        usuario:req.usuario._id,
    })

    

  let base64Image = producto.imagen.split(';base64,').pop();
  let image = producto.nombre.trim()+'-'+miliSecond +'.png'; 

  let pathImage = path.resolve(__dirname,  `../upload/productos/${image}` ) 

    fs.writeFile(pathImage, base64Image, {encoding: 'base64'},
     function(err) {
        
        if(err){
         return res.status(400).json({
                ok: false,
                mensaje: 'Error al cargar imagen',
                errors: err
            })
        }

        producto.imagen=image;


        producto.save((err, productoGuardado) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al crear producto',
                    errors: err
                })
            }
            res.status(201).json({
                ok: true,
                producto: productoGuardado,
                mensaje:'Producto registrado'
            });
        });

    });


  
});

/* Actualizar Producto */
app.put('/:id', mdAutentication.verificaToken, (req, res) => {
    var id = req.params.id
    var body = req.body;

    var date = new Date();
    var miliSecond = date.getMilliseconds();

    Producto.findById(id, (err, producto) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar Producto',
                erros: err
            });
        }

        if (!producto) {
            return res.status(400).json({
                ok: false,
                mensaje: 'El producto con el id: ' + id + 'no existe',
                erros: { message: 'No existe un producto con este ID' }
            });
        }

        
        var imagenVieja = producto.imagen;

        var pathViejo = path.resolve(__dirname,`../upload/productos/${imagenVieja}`) 

        var imagen= imagenVieja;

if(body.imagen !== imagenVieja){
         let base64Image = body.imagen.split(';base64,').pop();
          imagen = id+'-'+miliSecond+'.png';
          let pathImage = path.resolve(__dirname,`../upload/productos/${imagen}`) ;


           fs.writeFile(pathImage, base64Image, {encoding: 'base64'},
           function(err) {
              if(err){
               return res.status(400).json({
                      ok: false,
                      mensaje: 'Error al cargar imagen',
                      errors: err
                  })
              }

            });

        if (fs.existsSync(pathViejo)) {
            fs.unlink(pathViejo, (error) => {
    
              if (error) {
                return response.status(400).json({
                  ok: false,
                  mensaje: 'No se pudo eliminar la imagen',
                  errors: error
                });
              }
    
            });
          }
      
        }   

            
     
      
            producto.imagen=imagen;
            producto.nombre = body.nombre,
            //producto.imagen = body.imagen,
            producto.categoria = body.categoria,
            producto.precio = body.precio,
            producto.estatus = body.estatus,
            producto.ingrediente = body.ingrediente,
            producto.usuario =  req.usuario._id,
            
            producto.save((err, productoGuardado) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al actualizar producto',
                    erros: err
                });
            }

            res.status(200).json({
                ok: true,
                producto: productoGuardado,
                mensaje:'Producto Actualizado',
                usuariotoken: req.usuario
            });

        });

    
    });
});


/* Borrar producto por ID */
app.delete('/:id', mdAutentication.verificaToken, (req, res) => {
    var id = req.params.id

    Producto.findByIdAndRemove(id, (err, productoBorrado) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al borrar producto',
                errors: err
            })
        }

        if (!productoBorrado) {
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe un producto con ese ID',
                errors: { message: 'No existe un producto con ese ID' }
            })
        }

        res.status(200).json({
            ok: true,
            producto: productoBorrado,
            usuariotoken: req.usuario
        });
    })
});
module.exports = app;