var express = require('express');
var app = express();
var Motorizado = require('../models/motorizado');
var mdAutentication = require('../middlewares/autenticacion');

// Rutas 
app.get('/', (req, res, next) => {
    Motorizado.find({})
        .exec(
            (err, motorizados) => {

                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando motorizados',
                        errors: err
                    })

                } res.status(200).json({
                    ok: true,
                    motorizados: motorizados
                });

            })

});


/* Crear nuevos Motorizados */
app.post('/', mdAutentication.verificaToken, (req, res) => {
    var body = req.body;

    var date = new Date()


    let array = body.email;
    let claveRandom = '';
    for (var i = 0; i < 8; i++) {
        var rnd = Math.round(Math.random() * (array.length - 1));
        claveRandom += array[rnd];
    }

    var motorizado = new Motorizado({
        nombreApellido: body.nombreApellido,
        identificacion: body.identificacion,
        telefono: body.telefono,
        email: body.email,
        clave: claveRandom,
        fechaRegistro: date,
        vehiculo: body.vehiculo,
        estatus: 0,  // o Dispobible 1 Ocupado 2 fuera de servicio
    })

    motorizado.save((err, motorizadoGuardado) => {
        if (err) {
            return res.status(400).json({
                mensaje: 'Error al crear usuario',
                errors: err
            })
        }

        res.status(201).json({
            ok: true,
            mensaje: ' Motorizado ' + motorizadoGuardado.nombreApellido + ' registrado.',
        });


    });

});

module.exports = app;
