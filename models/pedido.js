var mongoose = require('mongoose');
var uniqueValidator =require('mongoose-unique-validator')
var Schema = mongoose.Schema;


var data = [ {  idProducto: String,
  proveedor: String,
  producto: String,
  precio: Number,
  cantidad: Number,
  ingredientes : Array,
  total: Number
}];

var pedidoSchema = new Schema({
    nControl: { type: String,unique:true, required: [true, 'El numero de control es necesario'] },
    direccionEntrega:{type: Schema.Types.ObjectId,ref:'Direccion', required: [false, 'La direccion es necesaria']},
    pedidos: {type: Array, required: [true, 'Debe agregar un pedido']},
    totalProductos:{type:Number, required: [true, 'Total productos es necesario']},
    totalDelivery :{type:Number, required: [true, 'Total Delivery es necesario']},
    totalArticulos :{type:Number, required: [true, 'Total Articulos es necesario']},
    estatus: { type:Number, required: [true, 'Estatus es necesario']}, //  Nuevo -> En proceso ->  Entregado
    encargadoDelivery: {type: Schema.Types.ObjectId,ref:'Motorizado'},
    usuario:{type: Schema.Types.ObjectId,ref:'Usuario', required: [true, 'El Usuario es necesario']},
    email: { type:String},
    formaDePago: {type:String, required: [true, 'Debe agregar una forma de pago.']},
    comprobante: {type:String},
    documento: {type:String},
    fechaPedido :{ type: String},


    
  },{ collection: 'pedido' });

pedidoSchema.plugin(uniqueValidator, { message: 'El numero de control ya existe'})

module.exports = mongoose.model('Pedido', pedidoSchema)


