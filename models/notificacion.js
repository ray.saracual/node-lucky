var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var notificacionSchema = new Schema({
    nombre: { type: String, require:[true, 'El Producto es necesario']},
    telefono: { type: Number}, 
    email: { type: String, required: [true, 'El email es necesario'] },
   tipoSolicitud : { type: String, required: [true, 'El tipo de solicitud es necesario'] },
   mensaje : { type: String, required: [true, 'El mensaje es necesario'] },
});

//productoSchema.plugin(uniqueValidator, { message: 'El correo ya existe'})

module.exports = mongoose.model('Notificacion', notificacionSchema)