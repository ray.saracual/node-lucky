var mongoose = require('mongoose');
var uniqueValidator =require('mongoose-unique-validator')
var Schema = mongoose.Schema; 

var proveedorSchema = new Schema({
    nombre: { type: String, required: [true, 'El nombre es necesario'] },
    telefono: { type: String,required: [true, 'El telefono es necesario'] },
    email : {type:String,unique:true},
    direccion:{type: Schema.Types.ObjectId,ref:'Direccion', required: [true, 'La dirección es necesaria']},
    estatus: { type: Number, required:[true, 'El estatus es necesario']}, 
    imagen: { type: String, required: [true, 'La imagen es necesaria'] },
    comisionAcobrar: { type: Number, required: [true, 'El comision es necesaria'] },
    observacion:{type: String},
    infoFiscal : {type:String,unique:true},

},{ collection: 'proveedor' });

proveedorSchema.plugin(uniqueValidator, { message: 'El {PATH} ya existe'})

module.exports = mongoose.model('Proveedor', proveedorSchema)