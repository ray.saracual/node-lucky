// Requires 
var express  = require ('express');
var mongoose = require ('mongoose');
var bodyParser = require('body-parser')
 var cors = require('cors')
 var mongodb = require("mongodb");

// Inicializar variables 
var app = express();

// CORS
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});


// Body Parser
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json({limit: '50mb'}))

// Conexion a base de datos
mongoose.connection.openUri(process.env.MONGODB_URI || 'mongodb://localhost:27017/fastZoomDB', { useNewUrlParser: true },(err, res) => {
    if ( err ) throw err;

    console.log(' Base de datos: \x1b[32m%s\x1b[0m',' online');
})

// importar Rutas
var imagenesRoutes = require('./routes/imagenes')
var motorizadoRoutes = require('./routes/motorizado')
var uploadRoutes = require('./routes/upload')
var afiliacionRoutes = require('./routes/afiliacion')
var direccionRoutes = require('./routes/direccion')
var categoriaRoutes = require('./routes/categoria')
var productoRoutes = require('./routes/producto')
var servicioRoutes = require('./routes/servicio')
var proveedorRoutes = require('./routes/proveedor')
var usuarioRoutes = require('./routes/usuario')
var loginRoutes = require('./routes/login')
var notificacionRoutes= require('./routes/notificacion')
var busquedaRoutes = require('./routes/busqueda')
var pedidoRoutes = require('./routes/pedido')
var kilometroRoutes = require('./routes/kilometro')
var appRoutes = require('./routes/app')

// Rutas 
app.use('/motorizado', motorizadoRoutes );
app.use('/imagen',imagenesRoutes );
app.use('/upload',uploadRoutes);
app.use('/afiliacion',afiliacionRoutes);
app.use('/direccion',direccionRoutes);
app.use('/pedido',pedidoRoutes);
app.use('/categoria',categoriaRoutes);
app.use('/notificacion',notificacionRoutes);
app.use('/producto', productoRoutes);
app.use('/servicio', servicioRoutes);
app.use('/proveedor', proveedorRoutes);
app.use('/usuario', usuarioRoutes);
app.use('/login', loginRoutes);
app.use('/busqueda', busquedaRoutes);
app.use('/kilometro',kilometroRoutes);
app.use('/', appRoutes); 

// Escuchar peticiones
app.listen( process.env.PORT || 3000, ()=> {
    console.log(' Express server puerto 3000: \x1b[32m%s\x1b[0m',' online');
} )
